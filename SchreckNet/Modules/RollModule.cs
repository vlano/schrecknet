﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;

// Keep in mind your module **must** be public and inherit ModuleBase.
// If it isn't, it will not be discovered by AddModulesAsync!
public class RollModule : ModuleBase<SocketCommandContext>
{
	[Command("roll")]
	[Summary("Rolls with the specified dice amount.")]
	public Task RollAsync(int all, int hunger)
	{
		string userName = Context.Guild.GetUser(Context.User.Id).Nickname;
		int[] hungerRolls = new int[hunger];
		int[] dTenRolls = new int[all-hunger];
		string dTenString = "";
		string hungerString = "";
		int succesCount = 0;
		int critCount = 0;
		bool messyCrit = false;
		bool bestialFail = false;

		List<int> allSuccessList = new List<int>();
		List<int> dTenSuccessList = new List<int>();
		List<int> hungerSuccessList = new List<int>();

		Random rand = new Random();
		

		for (int i = 0; i < all-hunger; i++)
		{
			dTenRolls[i] = rand.Next(0, 10);
			dTenString += dTenRolls[i] + ", ";
			if (dTenRolls[i] > 5 || dTenRolls[i] == 0)
			{
				dTenSuccessList.Add(dTenRolls[i]);
				allSuccessList.Add(dTenRolls[i]);
			}
		}

		for (int i = 0; i < hunger; i++)
		{
			hungerRolls[i] = rand.Next(0, 10);
			hungerString += hungerRolls[i] + ", ";
			if (hungerRolls[i] > 5 || hungerRolls[i] == 0)
			{
				hungerSuccessList.Add(hungerRolls[i]);
				allSuccessList.Add(hungerRolls[i]);
				if (hungerRolls[i] == 0 && dTenRolls.Contains(0))
					messyCrit = true;
			}
		}

		if (dTenSuccessList.Count == 0 && hungerSuccessList.Count == 0 && hungerRolls.Contains(1))
			bestialFail = true;

		succesCount = allSuccessList.Count;
		int baseCritCount = 0;
		critCount = GetCritCount(allSuccessList, out baseCritCount);

		if (baseCritCount > 1)
		{
			succesCount -= baseCritCount;
			succesCount += critCount;
		}

		string resultstring = $":bat: :bat: :bat: {userName} rolled {dTenString} with d10s and {hungerString} with hunger dice :bat: :bat: :bat: that is a total of {succesCount} successes";
		
		if (messyCrit)
			resultstring += " and a Messy Critical";
		else if (bestialFail)
			resultstring += " and a Bestial Failure";

		return ReplyAsync(resultstring);
	}

	private int GetCritCount(List<int> allSuccessList, out int tenCount)
	{
		tenCount = allSuccessList.Where(ten => ten.Equals(0)).Select(ten => ten).Count();
		int criticals = tenCount;
		if (criticals < 2)
			return criticals;

		if (criticals % 2 == 0)
			criticals *= 2;
		else
		{
			criticals *= 2;
			criticals--;
		}

		return criticals;
	}
}
